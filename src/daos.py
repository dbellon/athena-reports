# -*- coding: utf-8 -*-
import psycopg2

import configuration as configuration
import logger as logger
import sys

# ----------------------------------------------------------------------------------------------------------------------
# Needed variables
# ----------------------------------------------------------------------------------------------------------------------

log = logger.logger


# ----------------------------------------------------------------------------------------------------------------------
# Abstract DAO definition
# ----------------------------------------------------------------------------------------------------------------------

class AbstractDao:
    """
    A generic DAO definition that requires the database credentials and
    the database configuration
    """

    def __init__(self, username, password, configuration_dict, require_ssl):
        """
        Constructs a dao with a database connection,
        depending on the credentials given as construction arguments

        :param username: the database user
        :param password: the database password
        :param configuration_dict: a dictionary with the database configuration
        :param require_ssl: a boolean that defines if the connection needs ssl
        """

        try:
            log.info('Trying to establish connection with the %(db_name)s database',
                     {'db_name': configuration_dict['DB']})

            self.connection = psycopg2.connect(
                database=configuration_dict['DB'],
                host=configuration_dict['HOST'],
                port=configuration_dict['PORT'],
                user=username,
                password=password,
                sslmode='require',
                sslrootcert='../ssl/root.crt',
                sslcert='../ssl/server.crt',
                sslkey='../ssl/server.key'
            ) if require_ssl else psycopg2.connect(
                database=configuration_dict['DB'],
                host=configuration_dict['HOST'],
                port=configuration_dict['PORT'],
                user=username,
                password=password,
            )

            log.info('Connection with the database established')
        except psycopg2.Error:
            log.error('Unable to get a DB connection, please review your credentials and try again', exc_info=True)
            sys.exit()

    def close_connection(self):
        """
        Close the database connection
        """
        self.connection.close()


# ----------------------------------------------------------------------------------------------------------------------
# Custom DAO definitions
# ----------------------------------------------------------------------------------------------------------------------

class PayUDao(AbstractDao):
    """
    A DAO for read operations regarding the PayU database (polv4)
    """

    def __init__(self, username, password):
        """
        Constructs a dao with a database connection,
        depending on the credentials given as construction arguments
        """

        super().__init__(username, password, configuration.POLV4_DB, True)

        self.columns = ['transaccion_id', 'codigo_respuesta']

    def find_agreement_transactions(self, agreement_id, start_date, end_date):
        """
        Retrieve all the authorizations or authorization and captures for the given
        payment agreement

        :param agreement_id: the merchant for retrieve the legal representatives
        :param start_date: the initial date from which the transactions are gonna be retrieved
        :param end_date: the final date from which the transactions are gonna be retrieved
        :return: the payment agreement transactions (authorizations/authorization and captures)
        """

        log.info('Querying PayU transactions')

        query = """
            select 
                transaccion_id, 
                codigo_respuesta
            from pps.transaccion 
            where convenio_id = %(agreement_id)s
                and tipo in ('AUTHORIZATION_AND_CAPTURE','AUTHORIZATION')
                and fecha_creacion > %(start_date)s
                and fecha_creacion < %(end_date)s
        """

        cursor = self.connection.cursor()
        cursor.execute(query, {
            'agreement_id': agreement_id,
            'start_date': start_date,
            'end_date': end_date
        })

        return cursor.fetchall()


class RoutesDao(AbstractDao):
    """
    A DAO for read operations regarding the Route DB
    """

    def __init__(self, username, password):
        """
        Constructs a dao with a database connection,
        depending on the credentials given as construction arguments
        """

        super().__init__(username, password, configuration.ROUTES_DB, True)

        self.columns = ['transaccion_id', 'regla']

    def find_applied_rules_by_agreement(self, agreement_id, start_date, end_date):
        """
        Retrieve all applied rules for an specific payment agreement

        :param agreement_id: the merchant for retrieve the legal representatives
        :param start_date: the initial date from which the applied rules are gonna be retrieved
        :param end_date: the final date from which the applied rules are gonna be retrieved
        :return: the payment agreement applied rules
        """

        log.info('Querying Route API applied rules')

        query = """
            select distinct
                transaction_id as transaccion_id, 
                rule_name as regla                
            from route.applied_rule ar
            inner join route.rule r using (rule_id)
            inner join route.category c using (category_id)
            where ar.payment_agreement_id = %(agreement_id)s
                and ar.execution_date > %(start_date)s
                and ar.execution_date < %(end_date)s
        """

        cursor = self.connection.cursor()
        cursor.execute(query, {
            'agreement_id': agreement_id,
            'start_date': start_date,
            'end_date': end_date
        })

        return cursor.fetchall()


class MafDao(AbstractDao):
    """
    A DAO for read operations about the legal representatives for a merchant,
    querying the on-boarding database
    """

    def __init__(self, username, password):
        """
        Constructs a dao with a database connection,
        depending on the credentials given as construction arguments
        """

        super().__init__(username, password, configuration.MAF_DB, False)

        self.columns = [
            'transaccion_id',
            'transactions_by_email',
            'chargebacks_by_email',
            'is_multiparameter_white_list'
        ]

    def find_applied_rule_details(self, transaction_ids):
        """
        Retrieve all applied rules for an specific payment agreement

        :param transaction_ids: all the transactions thar needs the athena execution details
        :return: the details for the athena execution logic
        """

        log.info('Querying MAF for applied rules details')

        query = """
            select 
                transaction_id as transaccion_id,
                transactions_by_email,
                chargebacks_by_email,
                is_multiparameter_white_list
            from maf.athena_hard_rules_detail
            where transaction_id in %(transaction_ids)s
        """

        cursor = self.connection.cursor()
        cursor.execute(query, {'transaction_ids': transaction_ids})

        return cursor.fetchall()
