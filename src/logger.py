# -*- coding: utf-8 -*-
import datetime
import logging as log

# ----------------------------------------------------------------------------------------------------------------------
# Logs configuration
# ----------------------------------------------------------------------------------------------------------------------

TODAY = datetime.date.today()
LOG_NAME = f'logs/athena-out-{str(TODAY)}.log'

for handler in log.root.handlers[:]:
    log.root.removeHandler(handler)

log.basicConfig(
    format='%(asctime)s:%(levelname)s:%(name)s:%(message)s',
    filename=LOG_NAME,
    filemode='a+',
    level=log.INFO)

console = log.StreamHandler()
console.setLevel(log.INFO)
logger = log.getLogger('athena-out')
logger.addHandler(console)


# ----------------------------------------------------------------------------------------------------------------------
# Logs utils
# ----------------------------------------------------------------------------------------------------------------------

def separate():
    """
    Prints a log separator in order to improve readability
    """
    logger.warning('=' * 100)


def log_section(section_name):
    """
    Prints a section using the separate() method in order to improve
    the readability
    :param section_name: name of the section to be logged
    """
    separate()
    logger.warning(section_name)
    separate()
