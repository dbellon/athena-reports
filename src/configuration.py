# -*- coding: utf-8 -*-

POLV4_DB = dict(
    DB='pol_v4',
    HOST='10.50.101.15',
    PORT=5432
)

ROUTES_DB = dict(
    DB='route',
    HOST='10.50.101.15',
    PORT=5432
)

MAF_DB = dict(
    DB='maf',
    HOST='10.50.203.191',
    PORT=5433
)

COMPLETE_REPORT = dict(
    COLUMNS=[
        'transaccion_id',
        'codigo_respuesta',
        'regla',
        'transactions_by_email',
        'chargebacks_by_email',
        'is_multiparameter_white_list'
    ],
    HEADERS=[
        'Transaction ID',
        'Response Code',
        'Athena Result',
        'Transactions by Email',
        'Chargebacks by Email',
        'Is Multi-parameter White List'
    ]
)

SUMMARY_REPORT = dict(
    COLUMNS=[
        'rule',
        'approval_rate',
        'transactions',
        'approval_rate_percentage',
        'transactions_percentage'
    ],
    HEADERS=[
        'Rule',
        'Approved Transactions',
        'Total Transactions',
        'Approval Rate',
        'Transactions Percentage'
    ]
)
