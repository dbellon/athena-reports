# -*- coding: utf-8 -*-
import configuration
import pandas as pd
import logger as log
import exporter as exporter

from daos import PayUDao
from daos import RoutesDao
from daos import MafDao

# ----------------------------------------------------------------------------------------------------------------------
# Needed variables
# ----------------------------------------------------------------------------------------------------------------------

logger = log.logger


# ----------------------------------------------------------------------------------------------------------------------
# Script logic
# ----------------------------------------------------------------------------------------------------------------------

def request_inputs():
    """
    Ask for all the needed inputs for the report generation,
    including the database credentials and the report arguments

    :return username: the database username
    :return password: the database password
    :return payment_agreement: the id of the payment agreement for which the report is gonna be built
    :return start_date: the initial report date
    :return end_date: the final report date
    """

    logger.info('Please insert the payment agreement for which the report is gonna be built ...')
    payment_agreement = input('Payment agreement: ')

    logger.info('Please insert the report start date ...')
    start_date = input('Start date: ')

    logger.info('Please insert the report end date ...')
    end_date = input('End date: ')

    return payment_agreement, start_date, end_date


# ----------------------------------------------------------------------------------------------------------------------
# Reporter
# ----------------------------------------------------------------------------------------------------------------------

class Reporter:
    """
    Class able to generate Athena out using its DAOs,
    querying the 3 different needed data sources
    """

    # ------------------------------------------------------------------------------------------------------------------
    # Constructor
    # ------------------------------------------------------------------------------------------------------------------

    def __init__(self, username, password):
        """
        Build a Reporter instance requesting the database credentials
        in order to initialize the needed DAOs

        :param username: the database username
        :param password: the database password
        """
        self.username = username
        self.password = password
        self.payu_dao = PayUDao(username, password)
        self.routes_dao = RoutesDao(username, password)
        self.maf_dao = MafDao(username, password)

    # ------------------------------------------------------------------------------------------------------------------
    # Methods
    # ------------------------------------------------------------------------------------------------------------------

    # noinspection PyBroadException
    def generate_report(self):
        """
        The main script logic that defines all the processes
        that should be executed in order to generate the Athena out
        """

        try:

            # Requiring the report parameters
            payment_agreement, start_date, end_date = request_inputs()

            # Getting the base dataframes ...
            full_frame = self.__get_full_frame(payment_agreement, start_date, end_date)
            summary_frame = self.__get_summary_frame(full_frame)

            # Preparing dataframes to be exported ...
            full_report_df = self.__add_full_report_df_headers(full_frame)
            summary_report_df = self.__add_summary_report_df_headers(summary_frame)

            # Exporting dataframes ...
            exporter.export_frame_to_excel(full_report_df, 'Athena Full Report')
            exporter.export_frame_to_excel(summary_report_df, 'Athena Summary Report')

        except Exception:
            logger.error('Unable to generate report', exc_info=True)
        finally:
            self.__close_dao_connections()

    # ------------------------------------------------------------------------------------------------------------------
    # Inner logic
    # ------------------------------------------------------------------------------------------------------------------

    def __get_summary_frame(self, data_frame):
        """
        Receive a base dataframe and calculate the approval rate for each Athena rule type,
        and count the transactions by each Athena rule type

        :param data_frame: a base dataframe with the POLV4 and ROUTES API data
        :return: a dataframe with approval rates and transactions count for each Athena rule
        """

        transactions = len(data_frame)
        approval_rates_frame = self.__get_approval_rates_frame(data_frame)
        summary_frame = self.__get_transactions_by_rule_frame(data_frame)

        result = pd.merge(
            left=approval_rates_frame,
            right=summary_frame,
            on='rule',
            how='inner'
        )

        result['approval_rate_percentage'] = (100 * result['approval_rate']) / result['transactions']
        result['transactions_percentage'] = (100 * result['transactions']) / transactions

        return result

    # noinspection PyMethodMayBeStatic
    def __add_full_report_df_headers(self, data_frame):
        """
        Receive the dataframe with all the POLV4 and Routes API data in order
        to add it the headers row, preparing it to be exported

        :param data_frame: the base dataframe
        :return: a dataframe prepared to be exported
        """

        transaction_ids = tuple(data_frame['transaccion_id'].values.tolist())
        athena_details = self.maf_dao.find_applied_rule_details(transaction_ids)
        athena_details_frame = pd.DataFrame(athena_details, columns=self.maf_dao.columns)

        result_frame = pd.merge(
            left=data_frame,
            right=athena_details_frame,
            on='transaccion_id',
            how='left'
        )

        frame_headers = pd.DataFrame(columns=configuration.COMPLETE_REPORT['COLUMNS'])
        frame_headers.loc[0] = configuration.COMPLETE_REPORT['HEADERS']
        full_report_df = pd.concat([frame_headers, result_frame])
        return full_report_df

    # noinspection PyMethodMayBeStatic
    def __add_summary_report_df_headers(self, summary_frame):
        """
        Receives the dataframe with the summary results regarding the approval rates
        and transactions count by Athena rule in order to add it the headers row
        and prepare it to be exported

        :param summary_frame: the base dataframe
        :return: a dataframe prepared to be exported
        """
        summary_frame_headers = pd.DataFrame(columns=configuration.SUMMARY_REPORT['COLUMNS'])
        summary_frame_headers.loc[0] = configuration.SUMMARY_REPORT['HEADERS']
        summary_report_df = pd.concat([summary_frame_headers, summary_frame])
        return summary_report_df

    # noinspection PyMethodMayBeStatic
    def __get_transactions_by_rule_frame(self, data_frame):
        """
        Receives a base dataframe and calculate the transactions count
        by Athena rule tye

        :param data_frame: the base dataframe
        :return: a dataframe with transactions count by Athena rule
        """

        summary_series = data_frame['regla'].value_counts()
        summary_frame = pd.DataFrame(columns=['rule', 'transactions'])
        summary_frame['rule'] = summary_series.index
        summary_frame['transactions'] = summary_series.values
        return summary_frame

    # noinspection PyMethodMayBeStatic
    def __get_approval_rates_frame(self, data_frame):
        """
        Receives a base dataframe in order to calculate
        the approval rate for each Athena rule

        :param data_frame: a base dataframe
        :return: a dataframe with the approval rates for each Athena rule
        """

        base_frame = data_frame.groupby(['regla', 'codigo_respuesta'])

        low_approved = len(base_frame.get_group(('LOW', 'APPROVED')))
        medium_approved = len(base_frame.get_group(('MEDIUM', 'APPROVED')))
        unknown_approved = len(base_frame.get_group(('UNKNOWN', 'APPROVED')))

        approval_rates_frame = pd.DataFrame(columns=['rule', 'approval_rate'])
        approval_rates_frame['rule'] = ['LOW', 'MEDIUM', 'UNKNOWN']
        approval_rates_frame['approval_rate'] = [low_approved, medium_approved, unknown_approved]

        return approval_rates_frame

    def __get_full_frame(self, payment_agreement, start_date, end_date):
        """
        Receives the payment agreement Id, start and end date
        in order to build a data frame with all the needed data
        for the

        :param payment_agreement: the id of the payment agreement for which the report is gonna be built
        :param start_date: the initial report date
        :param end_date: the final report date
        :return: a pandas dataframe
        """

        logger.info('Building dataframe ...')

        payu_transactions = self.payu_dao.find_agreement_transactions(
            payment_agreement,
            start_date,
            end_date)

        applied_rules = self.routes_dao.find_applied_rules_by_agreement(
            payment_agreement,
            start_date,
            end_date)

        transactions_frame = pd.DataFrame(payu_transactions, columns=self.payu_dao.columns)
        applied_rules_frame = pd.DataFrame(applied_rules, columns=self.routes_dao.columns)

        data_frame = pd.merge(
            left=transactions_frame,
            right=applied_rules_frame,
            on='transaccion_id',
            how='inner')

        logger.info('Dataframe successfully built')

        return data_frame

    def __close_dao_connections(self):
        """
        Close all the connections opened by the needed DAOs
        """

        logger.info('Closing database connections')
        self.payu_dao.close_connection()
        self.routes_dao.close_connection()


# ----------------------------------------------------------------------------------------------------------------------
# Executable
# ----------------------------------------------------------------------------------------------------------------------

if __name__ == '__main__':
    log.log_section('Athena Reports')

    logger.info('Please insert the database username ...')
    username = input('Username: ')

    logger.info('Please insert the database password ...')
    password = input('Password: ')

    reporter = Reporter(username, password)
    reporter.generate_report()

    log.log_section('Athena Reports script has finished')
