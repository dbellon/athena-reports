# -*- coding: utf-8 -*-
import pandas as pd
from _datetime import datetime


def export_frame_to_excel(data_frame, file_name):
    """
    Export the given data frame to an excel file

    :param data_frame: the frame to be exported
    :param file_name: the desired report file name
    """
    date = str(datetime.now())
    file_name = f'../out/{file_name} - {date}.xlsx'
    writer = pd.ExcelWriter(file_name)
    data_frame.to_excel(writer, sheet_name='case', index=False, header=False)
    writer.save()
